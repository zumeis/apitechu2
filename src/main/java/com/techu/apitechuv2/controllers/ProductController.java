package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public List<ProductModel> getProducts() {
        System.out.println("getProducts");

        return productService.findAll();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findbyId(id);

//        if(result.isPresent() == true) {
//            return new ResponseEntity<>(result.get(), HttpStatus.OK);
//
//        } else {
//            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
//        }
        
        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La Descripción del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);

    }

}
